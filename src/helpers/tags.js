import React from 'react';

// Get the category that a tag belongs to (e.g. react is in the category 'js') for styling
const categories = {
    js: ['ts', 'react', 'vue'],
    design: ['css', 'design'],
    platform: ['web', 'electron', 'react-native'],
    solo: ['solo']
};

export function categoryFromTag(tag) {
    return Object.keys(categories).find(c => {
        return categories[c].includes(tag);
    });
}

export function getTags(arr) {
    const tagArray = [];
    arr.forEach(t => {
        const category = categoryFromTag(t);
        tagArray.push(
            <div
                key={t}
                className={`tag tag-${category}`}
            >
                {t}
            </div>
        );
    });
    return tagArray;
}