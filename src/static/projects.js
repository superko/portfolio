const projects = {
    multyband: {
        name: 'Multyband',
        categories: [
            'vue',
            'css',
            'design',
            'web',
            'solo'
        ],
        screenshot: 'multyband.png',
        summary: 'Single-page application for aggreggating and saving/sharing music from Bandcamp',
        description: [
            'A hobby project I did to dip my toe in Vue. This browser app lets you aggregate albums from Bandcamp onto a single page by entering the URLs of albums/songs. Then you can "export" your collection into a single URL, allowing you to share your list with anyone.',
            'The idea is that you can quickly and easily create a shareable collection: for brainstorming, moodboarding, or just to show your pals what you\'ve been into lately.'
        ],
        appUrl: 'https://oldhaunt.org/multyband/',
        codeUrl: 'https://gitlab.com/superko/multyband'
    },
    peerioApp: {
        name: 'Peerio (app)',
        categories: [
            'ts',
            'react',
            'css',
            'electron',
            'react-native'
        ],
        screenshot: 'peerio-app.png',
        summary: 'Cross-platform app for e2e encrypted messaging, group chat, and file sharing',
        description: [
            'Peerio is an app for secure collaboration. It draws influence from the likes of Slack, Dropbox, and Google Drive, but is entirely end-to-end encrypted.',
            'I\'ve worked at Peerio since August 2017 as a front end developer. I work primarily on the desktop app but make occasional forays into mobile.',
            'Some of my key contributions include: making the PeerUI component library, implementing the current signup flow, writing the backbone of our telemetry system, and overseeing a complete rebrand and visual overhaul of the app earlier this year.'
        ],
        appUrl: 'https://www.peerio.com/',
        codeUrl: 'https://github.com/PeerioTechnologies'
    },
    peerioWeb: {
        name: 'Peerio (website)',
        categories: [
            'css',
            'web'
        ],
        screenshot: 'peerio-web.png',
        summary: 'Peerio\'s website, a Hugo-generated static site.',
        description: [
            'For better performance and to circumvent some security concerns, Peerio opted to build their website using a static site generator instead of a conventional blogging CMS.',
            'I was responsible for converting the design mockups into Hugo-ready template files and ensuring compatibility with Forestry, the CMS system that our marketing team uses to publish content.'
        ],
        appUrl: 'https://www.peerio.com/'
    },
    peerUi: {
        name: 'PeerUI',
        categories: [
            'react',
            'ts',
            'css',
            'solo'
        ],
        screenshot: 'peerui.png',
        summary: 'React component library for UI elements used in Peerio\'s desktop application, soon to be deployed to web properties as well',
        description: [
            'Peerio\'s desktop app was originally built with a popular UI component library, but as the app evolved we found the library\'s built-in functionality limiting, and we were using a lot of ugly hacks to implement our features.',
            'Earlier this year I was tasked with building a custom UI library from scratch that would better suit our needs and could be modified or extended as needed. Originally contained in a folder in our desktop app repo, I was later able to extract it as a standalone repo, ready to be deployed in any web app.'
        ],
        codeUrl: 'https://github.com/PeerioTechnologies/ui-library'
    },
    peerUiWeb: {
        name: 'PeerUI Playground',
        categories: [
            'react',
            'css',
            'design',
            'web',
            'solo'
        ],
        screenshot: 'peerui-playground.png',
        summary: 'An in-browser playground for PeerUI in which the user can alter component properties and see how they affect the component\'s rendering',
        description: [
            'This was originally meant to be PeerUI\'s documentation, something that showed the user how to use the components in a way that was interactive and even accessible to non-developers.',
            'It became clear that keeping it up-to-date with PeerUI would be too heavy a task, so we\'ve moved on to something else. But the playground still works! You can fiddle with the very first working version of PeerUI.'
        ],
        appUrl: 'https://staging.superko.org/peerio/ui-playground/',
        codeUrl: 'https://github.com/suprko/ui-playground'
    }
};

module.exports = projects;