import React from 'react';
import css from 'classnames';
import { options, SelectionContext} from './selection-context.js';
import { categoryFromTag } from '../helpers/tags.js';

export default class Menu extends React.Component {
    constructor() {
        super();
        
        this.state = {
            open: false
        };

        this.toggleMenu = () => {
            this.setState({
                open: !this.state.open
            });
        }
    }

    get menuItems() {
        // Start with the 'all' toggle
        const menuSections = [
            <div className="menu-section menu-section-toggle-all" key="toggle-all">
                <MenuItem option="all" fullName="toggle all" />
            </div>
        ];

        options.forEach((arr, index) => {
            // Creates a "local" array of menu items, for this menu section
            const sectionItems = [];
            arr.forEach((o, i) => {
                sectionItems.push(
                    <MenuItem
                        key={o.option}
                        option={o.option}
                        // parent={i===0}
                        fullName={o.fullName || o.option}
                    />
                );
            });

            // Push this menu section to the "global" menuSections array
            menuSections.push(
                <div className={`menu-section menu-section-${index}`} key={arr[0].option}>
                    {sectionItems}
                </div>
            );
        });

        return menuSections;
    }

    render() {
        return(
            <>
                <div className="menu-buttons">
                    <div className="content-container">
                        <button className="icon-button" onClick={this.toggleMenu}><img src="./icon/sliders.svg" alt="Toggle filters"/></button>
                        <a className="icon-button" href="https://gitlab.com/superko/portfolio" target="_blank"><img src="./icon/code.svg" alt="View source code for this website" /></a>
                    </div>
                </div>
                <div className={css('menu', { open: this.state.open })}>
                    <div className="content-container">
                        {this.menuItems}
                        <div className="legend">(matches <span>OR</span>, not <span>AND</span>)</div>
                    </div>
                </div>
            </>
        );
    }
}

class MenuItem extends React.Component {
    get category() {
        return categoryFromTag(this.props.option);
    }

    render() {
        const {option, fullName} = this.props;
        return (
            <SelectionContext.Consumer>
                {(context) => {
                    const active = context.selected[option]
                    return (
                        <button
                            className={css(
                                'menu-item',
                                'tag',
                                `tag-${this.category}`,
                                { active }
                            )}
                            onClick={() => context.changeSelection(option)}
                        >
                            {fullName}
                        </button>
                    );
                }}
            </SelectionContext.Consumer>
        );
    }
}