import React from 'react';
import Card from './Card.jsx';
import projects from '../static/projects.js';

export default class Portfolio extends React.Component {
    get cards() {
        const cardsArray = [];
        
        Object.keys(projects).forEach(p => {
            cardsArray.push(
                <Card
                    key={p}
                    projectId={p}
                    name={projects[p].name}
                    tags={projects[p].categories}
                    screenshot={projects[p].screenshot}
                    summary={projects[p].summary}
                />
            );
        });

        return cardsArray;
    }

    render() {
        return(
            <div className="portfolio">
                {this.cards}
            </div>
        );
    }
}