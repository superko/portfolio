import React from 'react';
import Menu from './Menu.jsx';
import Popup from './Popup.jsx';
import Portfolio from './Portfolio.jsx';
import { SelectionContext, allSelected, noneSelected } from './selection-context.js';

export default class Index extends React.Component {
    constructor(props) {
        super(props);

        // changeSelection is finicky since we're changing a nested property inside the `selected` object.
        this.changeSelection = (option) => {
            // Catch 'toggle all' case
            if (option === 'all') {
                this.setState(
                    {
                        selected: this.areAllSelected
                            ? noneSelected
                            : allSelected
                    }
                );
            
            // Regular toggle
            } else {
                this.setState({
                    selected: {
                        ...this.state.selected,
                        [option]: !this.state.selected[option]
                    }
                });
            }

        };

        this.activatePopup = (project) => {
            this.setState({
                activePopup: project
            });
        };

        this.deactivatePopup = () => {
            this.setState({
                activePopup: ''
            });
        };

        this.state = {
            selected: allSelected, // All selected by default
            changeSelection: this.changeSelection,
            activePopup: '',
            activatePopup: this.activatePopup
        };
    }

    get areAllSelected() {
        // If there is a single deselected option, return false.
        // Otherwise, true.
        return !Object.keys(this.state.selected).find(o => {
            return this.state.selected[o] === false;
        });
    }

    render() {
        return(
            <SelectionContext.Provider value={this.state}>
                <div className="app">
                    <header><h1>Lucas Huang | front end developer | portfolio</h1></header>

                    <Portfolio />
                    <Menu />
                    {this.state.activePopup === ''
                        ? null
                        : <Popup
                            project={this.state.activePopup}
                            onClose={this.deactivatePopup}
                        />
                    }
                </div>
            </SelectionContext.Provider>
        )
    }
}