import React from 'react';
import { SelectionContext } from './selection-context.js';
import { getTags } from '../helpers/tags.js';

export default class Card extends React.Component {
    render() {
        return (
            <SelectionContext.Consumer>
                {(context) => {
                    // Card is active if none of its tags return false when looked up in `context.selected`
                    const active = this.props.tags.reduce((accum, tag) => {
                        return (accum || context.selected[tag]);
                    }, false);

                    return (
                        <div className="card-container">
                            <button
                                className="card"
                                disabled={!active}
                                onClick={() => context.activatePopup(this.props.projectId)}
                            >
                                <img src={`./img/${this.props.screenshot}`} />
                                <h3>{this.props.name}</h3>
                                <p>{this.props.summary}</p>
                                <div className="tags">
                                    {getTags(this.props.tags)}
                                </div>
                            </button>
                        </div>
                    );
                }}
            </SelectionContext.Consumer>
        );
    }
};