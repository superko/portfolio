import React from 'react';
import ReactDOM from 'react-dom';
import projects from '../static/projects.js';
import { getTags } from '../helpers/tags.js';
const appRoot = document.getElementById('root');

export default class Popup extends React.Component {
    componentWillMount() {
        document.body.style.overflowY = 'hidden';
        window.addEventListener('keyup', this.handleKey);
    }

    componentWillUnmount() {
        document.body.style.overflowY = 'auto';
        window.removeEventListener('keyup', this.handleKey);
    }

    constructor(props) {
        super(props);
        this.handleKey = (ev) => {
            console.log(ev);
            if (ev.key === 'Escape' || ev.keyCode === 27) {
                this.props.onClose();
            }
        }
    }

    render() {
        const { name, categories, screenshot, description, appUrl, codeUrl } = projects[this.props.project];
        
        const popup = <>
            <div className="overlay" onClick={this.props.onClose} />
            <dialog className="popup">
                <button className="icon-button close-button" onClick={this.props.onClose}>
                    <img src="./icon/x.svg" alt="Close" />
                </button>
                <div className="popup-content">
                    <div className="heading">
                        <div className="heading-and-links">
                            <h2>{name}</h2>
                        </div>
                        <div className="tags">{getTags(categories)}</div>
                    </div>
                    <img src={`./img/${screenshot}`} />
                    {description.map(p => {
                        return <p key={p.slice(0,8)}>
                            {p}
                        </p>;
                    })}

                    <div className="links">
                        {appUrl
                            ? <a className="icon-button" href={appUrl} target="_blank">
                                <img src="./icon/external-link.svg" alt="" />
                                <span>view project</span>
                            </a>
                            : null
                        }
                        {codeUrl
                            ? <a className="icon-button" href={codeUrl} target="_blank">
                                <img src="./icon/code.svg" alt="" />
                                <span>view source</span>
                            </a>
                            : null
                        }
                    </div>
                </div>
            </dialog>
        </>;
        return ReactDOM.createPortal(popup, appRoot);
    }
}