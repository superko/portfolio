import React from 'react';

// Options divided by category and with fullname
export const options = [
    [
        { option: 'ts', fullName: 'typescript' },
        { option: 'react' },
        { option: 'vue' }
    ],
    
    [   
        { option: 'css', fullName: 'sass/css' },
        { option: 'design' }
    ],

    [   
        { option: 'web' },
        { option: 'electron' },
        { option: 'react-native', fullName: 'react native' }
    ],

    [
        { option: 'solo', fullName: 'solo or primary author' }
    ]
];

// Keep track of which portfolio piece should be "active"
// via an object where each option name is a key, corresponding value is bool
let allSelectedObj = {};
let noneSelectedObj = {};
options.forEach(arr => {
    arr.forEach(o => {
        allSelectedObj[o.option] = true;
        noneSelectedObj[o.option] = false;
    });
});
export const allSelected = allSelectedObj;
export const noneSelected = noneSelectedObj;

export const SelectionContext = React.createContext({
    selected: allSelected,
    changeSelection: () => {},

    activePopup: '',
    activatePopup: () => {}
});