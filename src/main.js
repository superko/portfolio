import React from 'react';
import ReactDOM from 'react-dom';
import Index from './components/Index.jsx';

const appRoot = document.getElementById('root');

// Add/remove `.keyboard-nav` class based on user's tab press or mouse movement
const addKeynavClass = function(ev) {
    if (ev.key === 'Tab' || ev.keyCode === 9) {
        appRoot.className = 'keyboard-nav';
        window.addEventListener
    }
};
const removeKeynavClass = function() {
    appRoot.className = '';
}
window.addEventListener('keyup', addKeynavClass);
window.addEventListener('mousemove', removeKeynavClass);

ReactDOM.render(
    <Index />,
    appRoot
);