var path = require('path');
var webpack = require('webpack');

module.exports = {
    mode: 'none',
    entry: ['@babel/polyfill', './src/main.js'],
    output: {
        filename: 'main.js'
    },
    module: {
        rules: [
            {   
                test: /\.jsx?$/,
                include: [
                    path.resolve(__dirname, 'src'),
                ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                }
            },
        ]
    }
};
